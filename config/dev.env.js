'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  FIREBASE: {
    API_KEY: "'AIzaSyACHjtlQGdaFm1m2egiMZh0iYAYjECL7Y8'",
    AUTH_DOMAIN: "'cryptogigs-dev.firebaseapp.com'",
    DB_URL: "'https://cryptogigs-dev.firebaseio.com'",
    PROJECT_ID: "'cryptogigs-dev'",
    STORAGE_BUCKET: "'cryptogigs-dev.appspot.com'",
    MESSAGING_SENDER_ID: "'411087222465'"
  },
  GA_ID: "'UA-109915355-2'"
})
