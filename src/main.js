// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

/* Vue */
import Vue from 'vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'

Vue.use(VueResource)
Vue.config.productionTip = false

/* App sass */
import './assets/style/app.scss'

/* App component */
import App from './components/App.vue'

/* Auth plugin */
import Auth from './auth'
Vue.use(Auth)

/* Lodash */
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
Vue.use(VueLodash, lodash)

/* Google analytics */
import VueAnalytics from 'vue-analytics'
Vue.use(VueAnalytics, {
  id: process.env.GA_ID,
  checkDuplicatedScript: true,
  autoTracking: {
    exception: true
  },
  router
})

/* Validation */
import VeeValidate from 'vee-validate'
Vue.use(VeeValidate)

/* Modal */
import VModal from 'vue-js-modal'
Vue.use(VModal, {
  dialog: true,
  adaptive: true
})

/* Progress bar */
import VueProgressBar from 'vue-progressbar'
const progressBarOptions = {
  color: 'rgb(154, 102, 255)',
  failedColor: 'red',
  thickness: '3px'
}
Vue.use(VueProgressBar, progressBarOptions)

/* Autosize textarea */
import VueTextareaAutosize from 'vue-textarea-autosize'
Vue.use(VueTextareaAutosize)

/* Social share */
import SocialSharing from 'vue-social-sharing'
Vue.use(SocialSharing)

/* Edit metatags */
import VueHead from 'vue-head'
Vue.use(VueHead)

/* Firebase */
import firebase from 'firebase'
import 'firebase/firestore'
const firebaseConfig = {
  apiKey: process.env.FIREBASE.API_KEY,
  authDomain: process.env.FIREBASE.AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE.DB_URL,
  projectId: process.env.FIREBASE.PROJECT_ID,
  storageBucket: process.env.FIREBASE.STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE.MESSAGING_SENDER_ID
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  // Attach the Vue instance to the window,
  // so it's available globally.
  created: () => {
    firebase.initializeApp(firebaseConfig)
    window.Vue = this
  },
  router,
  store,
  render: h => h(App)
})
