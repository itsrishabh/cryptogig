import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    // Each of these routes are loaded asynchronously, when a user first navigates to each corresponding endpoint.
    // The route will load once into memory, the first time it's called, and no more on future calls.
    // This behavior can be observed on the network tab of your browser dev tools.
    {
      path: '/',
      name: 'home',
      component: resolve => {
        require(['@/components/home/Home.vue'], resolve)
      }
    },
    {
      path: '/privacy',
      name: 'privacy',
      component: resolve => {
        require(['@/components/common/Privacy.vue'], resolve)
      }
    },
    {
      path: '/terms',
      name: 'terms',
      component: resolve => {
        require(['@/components/common/Terms.vue'], resolve)
      }
    },
    {
      path: '/login',
      name: 'login',
      component: resolve => {
        require(['@/components/login/Login.vue'], resolve)
      }
    },
    {
      path: '/signup',
      name: 'signup',
      component: resolve => {
        require(['@/components/signup/Signup.vue'], resolve)
      },
      props: (route) => ({
        query: route.query.email
      })
    },
    {
      path: '/about',
      name: 'about',
      component: resolve => {
        require(['@/components/about/About.vue'], resolve)
      }
    },
    {
      path: '/create',
      name: 'create',
      component: resolve => {
        require(['@/components/create/Create.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/profile/:username',
      name: 'profile',
      component: resolve => {
        require(['@/components/profile/Profile.vue'], resolve)
      },
      props: true
    },
    {
      path: '/edit/profile',
      name: 'edit profile',
      component: resolve => {
        require(['@/components/profile/Edit.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/addresses',
      name: 'addresses',
      component: resolve => {
        require(['@/components/profile/Addresses.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/setup',
      name: 'setup',
      component: resolve => {
        require(['@/components/common/Setup.vue'], resolve)
      }
    },
    {
      path: '/how',
      name: 'how',
      component: resolve => {
        require(['@/components/common/How.vue'], resolve)
      }
    },
    {
      path: '/chats',
      name: 'chats',
      component: resolve => {
        require(['@/components/chat/Chats.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/chats/:username',
      name: 'user chat',
      component: resolve => {
        require(['@/components/chat/DM.vue'], resolve)
      },
      beforeEnter: guardRoute,
      props: true
    },
    {
      path: '/view/:id',
      name: 'view',
      component: resolve => {
        require(['@/components/view/View.vue'], resolve)
      },
      props: true
    }
  ]
})

function guardRoute (to, from, next) {
  const auth = router.app.$options.store.state.auth
  if (!auth.isLoggedIn) {
    next({
      path: '/login',
      query: {
        redirect: to.name
      }
    })
  } else {
    next()
  }
}

export default router
