/* Vue */
import Vue from 'vue'

/* Firebase */
import * as firebase from 'firebase'
import 'firebase/firestore'
import VueFire from 'vuefire'

const firebaseConfig = {
  apiKey: process.env.FIREBASE.API_KEY,
  authDomain: process.env.FIREBASE.AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE.DB_URL,
  projectId: process.env.FIREBASE.PROJECT_ID,
  storageBucket: process.env.FIREBASE.STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE.MESSAGING_SENDER_ID
}
firebase.initializeApp(firebaseConfig)
Vue.use(VueFire)

const db = firebase.firestore()
const users = db.collection('users')
const posts = db.collection('posts')
const comments = db.collection('comments')
const chats = db.collection('chats')

export default {
  firebase: {
    users,
    posts,
    comments,
    chats
  }
}
