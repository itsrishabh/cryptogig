// Vue
import Vue from 'vue'
import router from './router'
import store from './store'

import firebase from 'firebase'

export default {
  /**
   * Install the Auth class.
   *
   * Creates a Vue-resource http interceptor to handle automatically adding auth headers
   * and refreshing tokens. Then attaches this object to the global Vue (as Vue.auth).
   *
   * @param {Object} Vue The global Vue.
   * @param {Object} options Any options we want to have in our plugin.
   * @return {void}
   */
  install (Vue, options) {
    // Vue.http.interceptors.push((request, next) => {
    //   const token = store.state.auth.accessToken
    //   const hasAuthHeader = request.headers.has('Authorization')

    //   if (token && !hasAuthHeader) {
    //     this.setAuthHeader(request)
    //   }

    //   next((response) => {
    //     if (this._isInvalidToken(response)) {
    //       return this._refreshToken(request)
    //     }
    //   })
    // })

    Vue.prototype.$auth = Vue.auth = this
  },

  /**
   * Check current user
   *
   */
  checkCurrentUser () {
    return new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          const data = {
            emailVerified: user.emailVerified,
            username: user.displayName,
            photoURL: user.photoURL,
            uid: user.uid
          }
          // console.log(data)
          resolve(data)
        } else {
          reject()
        }
      })
    })
  },

  /**
   * Signup
   *
   * @param {Object.<string>} creds The username and password for logging in.
   * @param {string|null} redirect The name of the Route to redirect to.
   * @return {Promise}
   */
  signup (creds, file, redirect) {
    return new Promise((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(creds.email, creds.password)
      .then(user => {
        // Get current user
        const currentUser = firebase.auth().currentUser

        if (file) {
          // Todo: NOT HACK THIS
          let progress = 0
          Vue.prototype.$Progress.start()

          const storageRef = firebase.storage().ref(`avatars/${currentUser.uid}/${file.name}`)
          const task = storageRef.put(file)

          task.on('state_changed',
            // progress
            (snapshot) => {
              // console.log('progress', snapshot)
              progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              Vue.prototype.$Progress.set(progress)
            },
            // error
            (snapshot) => {
              console.error('error', snapshot)
            },
            // complete
            (snapshot) => {
              // console.log('complete')
              const updateData = {
                displayName: creds.username,
                photoURL: task.snapshot.downloadURL
              }
              currentUser
              .updateProfile(updateData)
              .then(() => {
                // console.log('Added username and photo')

                const userData = {
                  created: Date.now(),
                  emailVerified: currentUser.emailVerified,
                  username: currentUser.displayName,
                  photoURL: task.snapshot.downloadURL,
                  email: currentUser.email,
                  uid: currentUser.uid
                }

                this._storeUser(userData)

                Vue.prototype.$ga.event({
                  eventCategory: 'auth',
                  eventAction: 'completed_signup',
                  eventLabel: 'username',
                  eventValue: userData.username
                })

                firebase.firestore()
                .collection('users')
                .doc(userData.username)
                .set(userData)
                .then((docRef) => {
                  if (redirect) {
                    router.push({ name: redirect })
                  }
                  resolve(currentUser)
                })
                .catch((error) => {
                  reject(error)
                })
              })
              .catch(error => {
                // todo: if error then update the username later or re-try
                console.log(error)
                reject(error)
              })
            }
          )
        } else if (!file) {
          const updateData = {
            displayName: creds.username
          }
          currentUser
          .updateProfile(updateData)
          .then(() => {
            // console.log('Added username')

            const userData = {
              created: Date.now(),
              emailVerified: currentUser.emailVerified,
              username: currentUser.displayName,
              photoURL: currentUser.photoURL,
              email: currentUser.email,
              uid: currentUser.uid
            }

            this._storeUser(userData)

            Vue.prototype.$ga.event({
              eventCategory: 'auth',
              eventAction: 'completed_signup',
              eventLabel: 'username',
              eventValue: userData.username
            })

            firebase.firestore()
            .collection('users')
            .doc(userData.username)
            .set(userData)
            .then((docRef) => {
              if (redirect) {
                router.push({ name: redirect })
              }
              resolve(currentUser)
            })
            .catch((error) => {
              reject(error)
            })
          })
          .catch(error => {
            // todo: if error then update the username later or re-try
            console.log(error)
            reject(error)
          })
        }
      })
      .catch(error => {
        console.log('register error', error)
        reject(error)
      })
    })
  },

  /**
   * Login
   *
   * @param {Object.<string>} creds The username and password for logging in.
   * @param {string|null} redirect The name of the Route to redirect to.
   * @return {Promise}
   */
  login (creds, redirect) {
    return new Promise((resolve, reject) => {
      firebase.auth()
      .signInWithEmailAndPassword(creds.email, creds.password)
      .then((user) => {
        // console.log(user)

        const userData = {
          emailVerified: user.emailVerified,
          username: user.displayName,
          photoURL: user.photoURL,
          email: user.email,
          uid: user.uid
        }

        this._storeUser(userData)

        if (redirect) {
          router.push({ name: redirect })
        }

        Vue.prototype.$ga.event({
          eventCategory: 'auth',
          eventAction: 'completed_login',
          eventLabel: 'username',
          eventValue: user.displayName
        })

        resolve(user)
      })
      .catch((error) => {
        console.log(error)
        reject(error)
      })
    })
  },

  /**
   * Logout
   *
   * Clear all data in our Vuex store (which resets logged-in status) and redirect back
   * to login form.
   *
   * @return {void}
   */
  logout () {
    const user = store.state.user
    Vue.prototype.$ga.event({
      eventCategory: 'logout',
      eventAction: 'completed_logout',
      eventLabel: 'username',
      eventValue: user.username
    })
    setTimeout(() => {
      store.commit('CLEAR_ALL_DATA')
    }, 500)
    return firebase.auth().signOut()
    .then(loggedOut => {
      router.push({ name: 'home' })
    })
    .catch(error => {
      console.log(error)
      router.push({ name: 'home' })
    })
  },

  /**
   * Store tokens
   *
   * Update the Vuex store with the access/refresh tokens received from the response from
   * the Oauth2 server.
   *
   * @private
   * @param {Response} response Vue-resource Response instance from an OAuth2 server.
   *      that contains our tokens.
   * @return {void}
   */
  _storeUser (data) {
    // console.log(data)
    const auth = store.state.auth
    const user = data

    // console.log(auth, user)

    auth.isLoggedIn = true
    // auth.accessToken = response.body.access_token
    // auth.refreshToken = response.body.refresh_token
    // TODO: get user's name from response from Oauth server.
    // user.name = 'John Smith'

    store.commit('UPDATE_AUTH', auth)
    store.commit('UPDATE_USER', user)
  },

  // Todo: upload file helper
  _uploadFile (route, file) {
    const storageRef = firebase.storage().ref(route.toString() + file.name)
    const task = storageRef.put(file)
    const downloadURL = task.snapshot.downloadURL

    let response
    task.on('state_changed',
      // progress
      (snapshot) => {
        console.log('progress', snapshot)
        response = downloadURL
      },
      // error
      (snapshot) => {
        console.error('error', snapshot)
        response = false
      },
      // complete
      (snapshot) => {
        console.log('complete')
        response = true
      }
    )
    return response
  }
}
